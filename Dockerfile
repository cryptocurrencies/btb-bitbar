# ---- Base Node ----
FROM ubuntu:14.04 AS base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu trusty main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libssl-dev git-core libboost-all-dev libdb4.8-dev libdb4.8++-dev libminiupnpc-dev pkg-config sudo && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ----
FROM base as build
RUN git clone https://github.com/Crypto-Currency/bitbar.git /opt/bitbar && \
    cd /opt/bitbar/src && \
    make -f makefile.unix

# ---- Release ----
FROM ubuntu:14.04 AS release
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu trusty main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y libboost-all-dev libdb4.8 libdb4.8++ libminiupnpc-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r bitbar && useradd -r -m -g bitbar bitbar
RUN mkdir /data
COPY --from=build /opt/bitbar/src/bitbard /usr/local/bin/
RUN chown bitbar:bitbar /data
USER bitbar
VOLUME /data
EXPOSE 8777 9344
CMD ["/usr/local/bin/bitbard", "-datadir=/data", "-conf=/data/bitbar.conf", "-server", "-txindex", "-printtoconsole"]